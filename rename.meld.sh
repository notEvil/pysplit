#!/bin/bash -e
self=$(dirname "$(realpath "$0")")
python "$self/diff.py" "$1" "$2" meld --rename
