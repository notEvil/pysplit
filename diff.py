import pysplit
import pysplit.directory as p_directory
import pysplit.rename as p_rename
import argparse
import pathlib
import shlex
import shutil
import subprocess
import tempfile

parser = argparse.ArgumentParser()

parser.add_argument("first")
parser.add_argument("second")
parser.add_argument("command")
parser.add_argument("--rename", action="store_true", default=False)

arguments = parser.parse_args()

first_path = pathlib.Path(arguments.first)
second_path = pathlib.Path(arguments.second)

with tempfile.TemporaryDirectory() as temporary_path:
    t_first_path = pathlib.Path(temporary_path, first_path.name)
    t_second_path = pathlib.Path(temporary_path, second_path.name)

    if first_path.is_dir():
        shutil.copytree(first_path, t_first_path)
        shutil.copytree(second_path, t_second_path)

        p_directory.split(t_first_path, t_second_path, mismatches=arguments.rename)

    else:
        pysplit.split(first_path, t_first_path)
        pysplit.split(second_path, t_second_path)

    if arguments.rename:
        p_rename.adjust(t_first_path, t_second_path)

    _ = shlex.split(arguments.command) + [str(t_first_path), str(t_second_path)]
    assert subprocess.Popen(_).wait() == 0
