# pysplit

splits up Python code into nested blocks along class and function boundaries. Each block is then rendered using [Black](https://github.com/psf/black) with sub blocks replaced by `pass  # >>> {type} {path}`. This allows you to compare versions of Python projects block by block. In comparison, Git allows you to compare files line by line which makes it exceptionally difficult to assess movements of code blocks.

## Requirements

- Python 3.7+
- [Black](https://github.com/psf/black)

Install Black using `pip install --user black` or `sudo pip install black`.

## Quickstart

- Make sure the requirements are met/installed
- Clone this repository with `git clone https://gitlab.com/notEvil/pysplit.git`
- Run `./diff.sh "/path/to/first/file/or/directory" "/path/to/second/file/or/directory" meld` from within pysplit to compare two files or directories using [Meld](https://meldmerge.org/)
- or run `git difftool -x "/path/to/pysplit/meld.sh"` from within some Git repository to view changes using pysplit and Meld

## TODO

In case of positive feedback

- Add simple installation
- Improve documentation
