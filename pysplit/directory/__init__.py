import pysplit
import pathlib
import shutil
import tempfile


_BUFFER_SIZE = 4096


def split(path, matching_path, mismatches=False):
    if matching_path is None:
        paths = _get_paths(path)

    else:
        paths = _get_matching_paths(path, matching_path, mismatches)

    for path in paths:
        with tempfile.NamedTemporaryFile() as temporary_file:
            # copy to temporary file
            with open(path, "rb") as file:
                shutil.copyfileobj(file, temporary_file)

            temporary_file.flush()
            #

            path.unlink()  # delete file

            pysplit.split(pathlib.Path(temporary_file.name), path)


def _get_paths(path):
    for path in path.iterdir():
        if path.is_file() and path.name[-len(".py") :].lower() == ".py":
            yield path

        if path.is_dir():
            yield from _get_paths(path)


def _get_matching_paths(path, matching_path, mismatches):
    file_paths, directory_paths = _get_entries(path)
    matching_file_paths, matching_directory_paths = _get_entries(matching_path)

    for file_name in file_paths.keys() & matching_file_paths.keys():
        file_path = file_paths[file_name]
        matching_file_path = matching_file_paths[file_name]

        if not _has_file_changed(file_path, matching_file_path):
            continue

        yield file_path
        yield matching_file_path

    for directory_name in directory_paths.keys() & matching_directory_paths.keys():
        yield from _get_matching_paths(
            directory_paths[directory_name],
            matching_directory_paths[directory_name],
            mismatches,
        )

    if mismatches:
        for file_name in file_paths.keys() - matching_file_paths.keys():
            yield file_paths[file_name]

        for file_name in matching_file_paths.keys() - file_paths.keys():
            yield matching_file_paths[file_name]

        for directory_name in directory_paths.keys() - matching_directory_paths.keys():
            yield from _get_matching_paths(directory_paths[directory_name], None, True)

        for directory_name in matching_directory_paths.keys() - directory_paths.keys():
            yield from _get_matching_paths(
                None, matching_directory_paths[directory_name], True
            )


def _get_entries(path):
    if path is None:
        return {}, {}

    file_paths = {}
    directory_paths = {}

    for path in path.iterdir():
        if path.is_file() and path.name[-len(".py") :].lower() == ".py":
            file_paths[path.name] = path

        elif path.is_dir():
            directory_paths[path.name] = path

    return file_paths, directory_paths


def _has_file_changed(path, matching_path):
    stat = path.stat()
    matching_stat = matching_path.stat()

    if stat.st_size != matching_stat.st_size:
        return True

    if (
        stat.st_mtime_ns == matching_stat.st_mtime_ns
    ):  # and stat.st_size == matching_stat.st_size
        return False

    return not _are_file_contents_equal(path, matching_path)


def _are_file_contents_equal(path, matching_path):
    with open(path, "rb") as file, open(matching_path, "rb") as matching_file:
        while True:
            content = file.read(_BUFFER_SIZE)
            matching_content = matching_file.read(_BUFFER_SIZE)

            if len(content) != len(matching_content):
                return False

            if len(content) == 0:
                break

            if content != matching_content:
                return False

    return True
