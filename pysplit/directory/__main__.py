import pysplit.directory as p_directory
import argparse
import pathlib

parser = argparse.ArgumentParser()

parser.add_argument(
    "path", help="Path to directory. !! CAUTION: this directory is modified in place !!"
)
parser.add_argument(
    "--matching",
    help="Path to matching directory. !! CAUTION: this directory is modified in place !!",
)

arguments = parser.parse_args()


p_directory.split(
    pathlib.Path(arguments.path),
    None if arguments.matching is None else pathlib.Path(arguments.matching),
)
