import itertools
import pathlib
import shutil


_DISCARD_SCORE = (
    0.5  # between 0 and 1; rather arbitrary, should just prevent crazy false-positives
)


def adjust(first_path, second_path, flat=True):
    if flat:
        first_paths = dict(_get_flat_paths(first_path, first_path))
        second_paths = dict(_get_flat_paths(second_path, second_path))

    else:
        first_paths = dict(_get_paths(first_path))
        second_paths = dict(_get_paths(second_path))

    first_only_keys = set(first_paths.keys() - second_paths.keys())
    second_only_keys = set(second_paths.keys() - first_paths.keys())
    renames = {}

    while len(first_only_keys) != 0 and len(second_only_keys) != 0:
        score, first_key, second_key = max(
            (
                _compare(first_paths[first_key], second_paths[second_key]),
                first_key,
                second_key,
            )
            for first_key in first_only_keys
            for second_key in second_only_keys
        )
        if score < _DISCARD_SCORE:
            break

        renames[first_key] = second_key
        first_only_keys.remove(first_key)
        second_only_keys.remove(second_key)

    if not flat:
        # recurse into directories
        _ = ((key, key) for key in first_paths.keys() & second_paths.keys())
        for first_key, second_key in itertools.chain(_, renames.items()):
            first_sub_path = first_paths[first_key]
            second_sub_path = second_paths[second_key]
            if first_sub_path.is_dir() and second_sub_path.is_dir():
                adjust(first_sub_path, second_sub_path)

    # rename
    for first_key, second_key in renames.items():
        first_sub_path = first_paths[first_key]
        second_sub_path = second_paths[second_key]

        path = pathlib.Path(first_sub_path, pathlib.Path(first_path, second_key))
        path.parent.mkdir(parents=True, exist_ok=True)
        shutil.move(first_sub_path, path)


def _get_paths(path):
    yield from ((pathlib.Path(path.name), path) for path in path.iterdir())


def _get_flat_paths(path, base_path):
    for path in path.iterdir():
        if path.is_file():
            yield (path.relative_to(base_path), path)

        elif path.is_dir():
            yield from _get_flat_paths(path, base_path)


def _compare(first_path, second_path):
    if first_path.is_file() and second_path.is_file():
        with open(first_path, "rb") as file:
            first_lines = set(file)
        with open(second_path, "rb") as file:
            second_lines = set(file)
        return len(first_lines & second_lines) / max(
            len(first_lines), len(second_lines)
        )  # simply the proportion of matching lines

    if first_path.is_dir() and second_path.is_dir():
        first_paths = {path.name: path for path in first_path.iterdir()}
        second_paths = {path.name: path for path in second_path.iterdir()}

        names = first_paths.keys() & second_paths.keys()
        return (
            1  # len(names)
            / max(len(first_paths), len(second_paths))
            * sum(_compare(first_paths[name], second_paths[name]) for name in names)
            # / len(names)
        )  # simply the proportion of matching names times the average proportion of matching lines

    return 0
