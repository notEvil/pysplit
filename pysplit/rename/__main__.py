import pysplit.rename as p_rename
import argparse
import pathlib


parser = argparse.ArgumentParser()

parser.add_argument(
    "first",
    help="Path to first directory. !! CAUTION: this directory is modified in place !!",
)
parser.add_argument(
    "second",
    help="Path to second directory. !! CAUTION: this directory is modified in place !!",
)

arguments = parser.parse_args()


p_rename.adjust(pathlib.Path(arguments.first), pathlib.Path(arguments.second))
