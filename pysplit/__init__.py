import ast
import itertools
import pathlib
import re


class _NodeVisitor(ast.NodeVisitor):
    """
    - identifies blocks

    Assumes:
    - line number of nodes increases monotonically
    """

    _NODE_TYPE_NAMES = {ast.FunctionDef: "def", ast.ClassDef: "class"}

    def __init__(self, lines):
        super().__init__()

        self.lines = lines

        self._root_block = None
        self._block_path = None
        self._open_blocks = None

    def visit_Module(self, node):
        self._root_block = _Block(from_line_number=1, type_name="module", sub_blocks=[])
        self._block_path = [self._root_block]
        self._open_blocks = []

        self.generic_visit(node)  # visit sub nodes

        self._block_path = self._open_blocks = None

    def visit_FunctionDef(self, node):
        block_path = self._block_path
        block = _Block(
            from_line_number=self._get_line_number(node),
            type_name=type(self)._NODE_TYPE_NAMES[type(node)],
            name=node.name,
            block_path=block_path,
            sub_blocks=[],
        )

        self._block_path = block_path + [block]
        self.generic_visit(node)  # visit sub nodes
        self._block_path = block_path

        self._open_blocks.append(block)
        block_path[-1].sub_blocks.append(block)

    def visit_ClassDef(self, node):
        block_path = self._block_path
        block = _Block(
            from_line_number=self._get_line_number(node),
            type_name=type(self)._NODE_TYPE_NAMES[type(node)],
            name=node.name,
            block_path=block_path,
            sub_blocks=[],
        )

        self._block_path = block_path + [block]
        self.generic_visit(node)
        self._block_path = block_path

        self._open_blocks.append(block)
        block_path[-1].sub_blocks.append(block)

    def generic_visit(self, node):
        self._close_open_blocks(node)

        super().generic_visit(node)  # visit sub nodes

    def _close_open_blocks(self, node):
        if len(self._open_blocks) == 0:
            return

        line_number = self._get_line_number(node)
        if line_number is None:
            return

        for block in self._open_blocks:
            to_line_number = line_number - 1

            # find correct to line number
            while True:
                line = self.lines[to_line_number - 1]

                match = re.search(
                    b"^((\s*)|(\s*(else|finally):\s*))$", line
                )  # TODO why else, finally
                if match is None:  # skip empty lines and separating lines without node
                    break

                to_line_number -= 1
            #

            block.to_line_number = to_line_number

        self._open_blocks.clear()

    def _get_line_number(self, node):
        if (
            isinstance(node, (ast.FunctionDef, ast.ClassDef))
            and len(node.decorator_list) != 0
        ):
            return node.decorator_list[0].lineno
        return getattr(node, "lineno", None)


class _Block:
    """
    - holds information about a block
    """

    def __init__(
        self,
        from_line_number=None,
        to_line_number=None,
        type_name=None,
        name=None,
        block_path=None,
        sub_blocks=None,
    ):
        self.from_line_number = from_line_number
        self.to_line_number = to_line_number
        self.type_name = type_name
        self.name = name
        self.block_path = block_path
        self.sub_blocks = sub_blocks

    def to_builtins(self):
        return dict(
            from_line_number=self.from_line_number,
            to_line_number=self.to_line_number,
            type_name=self.type_name,
            name=self.name,
            block_path=[block.name for block in self.block_path],
            sub_blocks=None
            if self.sub_blocks is None
            else [sub_block.to_builtins() for sub_block in self.sub_blocks],
        )

    def get_items(self):
        """
        - keys must not be unique
        """

        _ = (
            None
            if self.block_path is None
            else tuple(block.name for block in self.block_path)
        )
        yield ((_, self.type_name, self.name), self)

        if self.sub_blocks is not None:
            for sub_block in self.sub_blocks:
                yield from sub_block.get_items()


def _format_lines(lines, block):
    """
    - subsets lines
    - dedents lines
    - replaces sub blocks with placeholder
    """
    _ = len(lines) if block.to_line_number is None else block.to_line_number
    lines = lines[block.from_line_number - 1 : _]

    lines = _dedent_lines(lines)

    for sub_block in sorted(
        block.sub_blocks, key=lambda block: block.from_line_number, reverse=True
    ):
        _ = (
            len(lines)
            if sub_block.to_line_number is None
            else (sub_block.to_line_number - block.from_line_number + 1)
        )
        lines[sub_block.from_line_number - block.from_line_number : _] = [
            _get_indent(lines[sub_block.from_line_number - block.from_line_number])
            + "pass  # >>> {} {}".format(sub_block.type_name, sub_block.name).encode()
        ]

    return lines


def _dedent_lines(lines):
    if len(lines) == 0:
        return lines

    indent = _get_indent(lines[0])
    return [line[len(indent) :] if line.startswith(indent) else line for line in lines]


def _get_indent(line):
    return re.search(b"^[ \t]*", line).group(0)


def _format_block(block, lines):
    """
    - formats lines
    - formats source using black
    """
    import black

    source = b"\n".join(_format_lines(lines, block)).decode()
    result = black.format_str(source, mode=black.FileMode())
    return result


def split(path, to_path):
    to_path.mkdir(parents=True, exist_ok=True)

    with open(path, "rb") as file:
        source = file.read()

    lines = source.splitlines()

    visitor = _NodeVisitor(lines)
    visitor.visit(ast.parse(source))

    unique_names = set()
    for key, block in visitor._root_block.get_items():
        _write_to_file(key, block, unique_names, lines, to_path)


def _write_to_file(key, block, unique_names, lines, to_path):
    name_path, _, name = key

    if name_path is None:
        name = "__main__"

    else:
        assert name_path[0] is None
        parts = list(name_path[1:])
        parts.append(name)
        name = ".".join(parts)

    name = _get_unique_name(name, unique_names)

    with open(pathlib.Path(to_path, name), "wb") as file:
        file.write(_format_block(block, lines).encode())


def _get_unique_name(name, unique_names):
    if name not in unique_names:
        unique_names.add(name)
        return name

    for index in itertools.count(2):
        next_name = "{}_{}".format(name, index)
        if next_name not in unique_names:
            unique_names.add(next_name)
            return next_name
