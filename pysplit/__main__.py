import pysplit
import argparse
import pathlib


parser = argparse.ArgumentParser()

parser.add_argument("path", help="Path to Python file")
parser.add_argument("out", help="Path to output directory")

arguments = parser.parse_args()


pysplit.split(pathlib.Path(arguments.path), pathlib.Path(arguments.out))
